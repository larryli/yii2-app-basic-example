<?php

namespace tests\codeception\_pages;

use yii\codeception\BasePage;

/**
 * Represents login page
 * @property \AcceptanceTester|\FunctionalTester $actor
 */
class FooCreatePage extends BasePage
{
    public $route = 'foo/create';

    /**
     * @param string $name
     */
    public function create($name)
    {
        $this->actor->fillField('input[name="Foo[name]"]', $name);
        $this->actor->click('.btn-success');
    }
}
