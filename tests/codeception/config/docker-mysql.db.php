<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=mysql;dbname=yii2basic_tests',
    'username' => 'root',
    'password' => 'mysql',
    'charset' => 'utf8',
];
