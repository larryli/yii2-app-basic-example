<?php

use tests\codeception\_pages\FooCreatePage;

/* @var $scenario Codeception\Scenario */

$I = new AcceptanceTester($scenario);
$I->wantTo('ensure that foo create');

$createPage = FooCreatePage::openBy($I);

$I->see('Create Foo', 'h1');

$I->amGoingTo('try to create foo with empty name');
$createPage->create('');
if (method_exists($I, 'wait')) {
    $I->wait(3); // only for selenium
}
$I->expectTo('see validations errors');
$I->see('Name cannot be blank.');

$I->amGoingTo('try to create foo with empty name');
$createPage->create('foo');
if (method_exists($I, 'wait')) {
    $I->wait(3); // only for selenium
}
$I->expectTo('see foo');
$I->see('Foo', 'h1');
